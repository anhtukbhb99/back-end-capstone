/**
 * 
 */
package capstone.repository;

import capstone.entity.Potential;

/**
 * PotentialRepository
 * @author Tuna
 *
 */
public interface PotentialRepository extends NamedJpaRepository<Potential, Long> {

}
