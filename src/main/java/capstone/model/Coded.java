/**
 * 
 */
package capstone.model;

/**
 * Coded
 * @author Tuna
 *
 */
public interface Coded {
	
	/**
	 * Getter for code
	 * @return
	 */
	String getCode();

}
