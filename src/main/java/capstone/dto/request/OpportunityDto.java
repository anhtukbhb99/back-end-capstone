/**
 * 
 */
package capstone.dto.request;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import capstone.common.enums.OpportunityPhase;
import capstone.dto.request.deserializer.LocalDateDeserializer;
import capstone.dto.response.serializer.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Cơ hội dto
 * @author Tuna
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class OpportunityDto extends NamedDto<Long> {
	
	/**
	 * Khách hàng
	 */
	private Long customerId;
	
	/**
	 * Liên hệ
	 */
	private Long contactId;

	/**
	 * Gian đoạn
	 */
	@NotNull
	@JsonProperty("opportunityPhaseId") //
	@JsonAlias("opportunityPhaseId")
	private OpportunityPhase opportunityPhase;

	/**
	 * Tỷ lệ thành công
	 */
	@NotNull
	@javax.validation.constraints.Min(value = 0)
	@javax.validation.constraints.Max(value = 100)
	private Integer successRate;

	/**
	 * Ngày kỳ vọng kết thúc
	 */
	@NotNull
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate expectedEndDate;
	
	/**
	 * Nguồn gốc
	 */
	private Long sourceId;
	
	/**
	 * Thông tin hàng hóa
	 */
	private Set<ProductInfoDto> productInfoDtos;

	/**
	 * @param id
	 * @param name
	 * @param customerId
	 * @param contactId
	 * @param moneyAmount
	 * @param opportunityPhase
	 * @param successRate
	 * @param expectedEndDate
	 * @param expectedTurnOver
	 * @param sourceId
	 * @param productInfoDtos
	 */
	@Builder
	public OpportunityDto(Long id, String name, Long customerId, Long contactId, OpportunityPhase opportunityPhase,
			Integer successRate, LocalDate expectedEndDate, Long sourceId, Set<ProductInfoDto> productInfoDtos) {
		super(id, name);
		this.customerId = customerId;
		this.contactId = contactId;
		this.opportunityPhase = opportunityPhase;
		this.successRate = successRate;
		this.expectedEndDate = expectedEndDate;
		this.sourceId = sourceId;
		this.productInfoDtos = productInfoDtos;
	}

}
